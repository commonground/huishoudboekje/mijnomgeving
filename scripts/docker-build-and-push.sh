#!/bin/bash

set -e
# This file requires envvars CI_REGISTRY_IMAGE, IMAGE_TAG and DOCKER_PROXY to be set.
# If you are using GitLab, $CI_REGISTRY_IMAGE should already be set.

# Build docker images for all components
docker build -t $CI_REGISTRY_IMAGE/backendburgers:$IMAGE_TAG         --build-arg "DOCKER_PROXY=$DOCKER_PROXY"   ./backend-burgers

# Push all docker images to the registry
docker push $CI_REGISTRY_IMAGE/backendburgers:$IMAGE_TAG
