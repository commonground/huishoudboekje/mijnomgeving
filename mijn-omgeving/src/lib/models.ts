export type HuishoudboekjeUser = {
	bsn: number
}

export type HuishoudboekjeConfig = {
	apiUrl: string,
}