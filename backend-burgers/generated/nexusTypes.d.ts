/**
 * This file was generated by Nexus Schema
 * Do not make changes to this file directly
 */


import type { Context } from "./../src/context"




declare global {
  interface NexusGen extends NexusGenTypes {}
}

export interface NexusGenInputs {
}

export interface NexusGenEnums {
  DayOfWeek: "Friday" | "Monday" | "Saturday" | "Sunday" | "Thursday" | "Tuesday" | "Wednesday"
}

export interface NexusGenScalars {
  String: string
  Int: number
  Float: number
  Boolean: boolean
  ID: string
  Bedrag: any
  JSON: any
}

export interface NexusGenObjects {
  Afdeling: { // root type
    id?: number | null; // Int
    naam?: string | null; // String
  }
  Afspraak: { // root type
    bedrag?: string | null; // String
    credit?: boolean | null; // Boolean
    id?: number | null; // Int
    omschrijving?: string | null; // String
  }
  Banktransactie: { // root type
    bedrag?: NexusGenScalars['Bedrag'] | null; // Bedrag
    id?: number | null; // Int
  }
  Betaalinstructie: { // root type
    byDay?: Array<NexusGenEnums['DayOfWeek'] | null> | null; // [DayOfWeek]
    byMonth?: Array<number | null> | null; // [Int]
    byMonthDay?: Array<number | null> | null; // [Int]
    endDate?: string | null; // String
    exceptDates?: Array<string | null> | null; // [String]
    repeatFrequency?: string | null; // String
    startDate?: string | null; // String
  }
  Burger: { // root type
    achternaam?: string | null; // String
    bsn?: string | null; // String
    email?: string | null; // String
    geboortedatum?: string | null; // String
    huisnummer?: string | null; // String
    id?: number | null; // Int
    plaatsnaam?: string | null; // String
    postcode?: string | null; // String
    straatnaam?: string | null; // String
    telefoonnummer?: string | null; // String
    voorletters?: string | null; // String
    voornamen?: string | null; // String
  }
  Journaalpost: { // root type
    id?: number | null; // Int
  }
  Organisatie: { // root type
    id?: number | null; // Int
    kvknummer?: string | null; // String
    naam?: string | null; // String
    vestigingsnummer?: string | null; // String
  }
  PageInfo: { // root type
    count?: number | null; // Int
    limit?: number | null; // Int
    start?: number | null; // Int
  }
  PagedBanktransactie: { // root type
    banktransacties?: Array<NexusGenRootTypes['Banktransactie'] | null> | null; // [Banktransactie]
    pageInfo?: NexusGenRootTypes['PageInfo'] | null; // PageInfo
  }
  Query: {};
  Rekening: { // root type
    iban?: string | null; // String
    id?: number | null; // Int
    rekeninghouder?: string | null; // String
  }
}

export interface NexusGenInterfaces {
}

export interface NexusGenUnions {
}

export type NexusGenRootTypes = NexusGenObjects

export type NexusGenAllTypes = NexusGenRootTypes & NexusGenScalars & NexusGenEnums

export interface NexusGenFieldTypes {
  Afdeling: { // field return type
    id: number | null; // Int
    naam: string | null; // String
    organisatie: NexusGenRootTypes['Organisatie'] | null; // Organisatie
  }
  Afspraak: { // field return type
    bedrag: string | null; // String
    betaalinstructie: NexusGenRootTypes['Betaalinstructie'] | null; // Betaalinstructie
    credit: boolean | null; // Boolean
    id: number | null; // Int
    journaalposten: Array<NexusGenRootTypes['Journaalpost'] | null> | null; // [Journaalpost]
    omschrijving: string | null; // String
    tegenrekening: NexusGenRootTypes['Rekening'] | null; // Rekening
    validFrom: string | null; // String
    validThrough: string | null; // String
  }
  Banktransactie: { // field return type
    bedrag: NexusGenScalars['Bedrag'] | null; // Bedrag
    id: number | null; // Int
    informationToAccountOwner: string | null; // String
    isCredit: boolean | null; // Boolean
    journaalpost: NexusGenRootTypes['Journaalpost'] | null; // Journaalpost
    tegenrekening: NexusGenRootTypes['Rekening'] | null; // Rekening
    tegenrekeningIban: string | null; // String
    transactiedatum: string | null; // String
  }
  Betaalinstructie: { // field return type
    byDay: Array<NexusGenEnums['DayOfWeek'] | null> | null; // [DayOfWeek]
    byMonth: Array<number | null> | null; // [Int]
    byMonthDay: Array<number | null> | null; // [Int]
    endDate: string | null; // String
    exceptDates: Array<string | null> | null; // [String]
    repeatFrequency: string | null; // String
    startDate: string | null; // String
  }
  Burger: { // field return type
    achternaam: string | null; // String
    afspraak: NexusGenRootTypes['Afspraak'] | null; // Afspraak
    afspraken: Array<NexusGenRootTypes['Afspraak'] | null> | null; // [Afspraak]
    banktransacties: Array<NexusGenRootTypes['Banktransactie'] | null> | null; // [Banktransactie]
    banktransactiesPaged: NexusGenRootTypes['PagedBanktransactie'] | null; // PagedBanktransactie
    bsn: string | null; // String
    email: string | null; // String
    geboortedatum: string | null; // String
    huisnummer: string | null; // String
    id: number | null; // Int
    plaatsnaam: string | null; // String
    postcode: string | null; // String
    rekeningen: Array<NexusGenRootTypes['Rekening'] | null> | null; // [Rekening]
    straatnaam: string | null; // String
    telefoonnummer: string | null; // String
    voorletters: string | null; // String
    voornamen: string | null; // String
  }
  Journaalpost: { // field return type
    afspraak: NexusGenRootTypes['Afspraak'] | null; // Afspraak
    banktransactie: NexusGenRootTypes['Banktransactie'] | null; // Banktransactie
    id: number | null; // Int
  }
  Organisatie: { // field return type
    id: number | null; // Int
    kvknummer: string | null; // String
    naam: string | null; // String
    vestigingsnummer: string | null; // String
  }
  PageInfo: { // field return type
    count: number | null; // Int
    limit: number | null; // Int
    start: number | null; // Int
  }
  PagedBanktransactie: { // field return type
    banktransacties: Array<NexusGenRootTypes['Banktransactie'] | null> | null; // [Banktransactie]
    pageInfo: NexusGenRootTypes['PageInfo'] | null; // PageInfo
  }
  Query: { // field return type
    banktransactie: NexusGenRootTypes['Banktransactie'] | null; // Banktransactie
    burger: NexusGenRootTypes['Burger'] | null; // Burger
    burgers: Array<NexusGenRootTypes['Burger'] | null> | null; // [Burger]
  }
  Rekening: { // field return type
    afdelingen: Array<NexusGenRootTypes['Afdeling'] | null> | null; // [Afdeling]
    iban: string | null; // String
    id: number | null; // Int
    rekeninghouder: string | null; // String
  }
}

export interface NexusGenFieldTypeNames {
  Afdeling: { // field return type name
    id: 'Int'
    naam: 'String'
    organisatie: 'Organisatie'
  }
  Afspraak: { // field return type name
    bedrag: 'String'
    betaalinstructie: 'Betaalinstructie'
    credit: 'Boolean'
    id: 'Int'
    journaalposten: 'Journaalpost'
    omschrijving: 'String'
    tegenrekening: 'Rekening'
    validFrom: 'String'
    validThrough: 'String'
  }
  Banktransactie: { // field return type name
    bedrag: 'Bedrag'
    id: 'Int'
    informationToAccountOwner: 'String'
    isCredit: 'Boolean'
    journaalpost: 'Journaalpost'
    tegenrekening: 'Rekening'
    tegenrekeningIban: 'String'
    transactiedatum: 'String'
  }
  Betaalinstructie: { // field return type name
    byDay: 'DayOfWeek'
    byMonth: 'Int'
    byMonthDay: 'Int'
    endDate: 'String'
    exceptDates: 'String'
    repeatFrequency: 'String'
    startDate: 'String'
  }
  Burger: { // field return type name
    achternaam: 'String'
    afspraak: 'Afspraak'
    afspraken: 'Afspraak'
    banktransacties: 'Banktransactie'
    banktransactiesPaged: 'PagedBanktransactie'
    bsn: 'String'
    email: 'String'
    geboortedatum: 'String'
    huisnummer: 'String'
    id: 'Int'
    plaatsnaam: 'String'
    postcode: 'String'
    rekeningen: 'Rekening'
    straatnaam: 'String'
    telefoonnummer: 'String'
    voorletters: 'String'
    voornamen: 'String'
  }
  Journaalpost: { // field return type name
    afspraak: 'Afspraak'
    banktransactie: 'Banktransactie'
    id: 'Int'
  }
  Organisatie: { // field return type name
    id: 'Int'
    kvknummer: 'String'
    naam: 'String'
    vestigingsnummer: 'String'
  }
  PageInfo: { // field return type name
    count: 'Int'
    limit: 'Int'
    start: 'Int'
  }
  PagedBanktransactie: { // field return type name
    banktransacties: 'Banktransactie'
    pageInfo: 'PageInfo'
  }
  Query: { // field return type name
    banktransactie: 'Banktransactie'
    burger: 'Burger'
    burgers: 'Burger'
  }
  Rekening: { // field return type name
    afdelingen: 'Afdeling'
    iban: 'String'
    id: 'Int'
    rekeninghouder: 'String'
  }
}

export interface NexusGenArgTypes {
  Burger: {
    afspraak: { // args
      id: number; // Int!
    }
    banktransactiesPaged: { // args
      limit: number; // Int!
      start: number; // Int!
    }
  }
  Query: {
    banktransactie: { // args
      id: number; // Int!
    }
    burger: { // args
      bsn?: number | null; // Int
    }
  }
}

export interface NexusGenAbstractTypeMembers {
}

export interface NexusGenTypeInterfaces {
}

export type NexusGenObjectNames = keyof NexusGenObjects;

export type NexusGenInputNames = never;

export type NexusGenEnumNames = keyof NexusGenEnums;

export type NexusGenInterfaceNames = never;

export type NexusGenScalarNames = keyof NexusGenScalars;

export type NexusGenUnionNames = never;

export type NexusGenObjectsUsingAbstractStrategyIsTypeOf = never;

export type NexusGenAbstractsUsingStrategyResolveType = never;

export type NexusGenFeaturesConfig = {
  abstractTypeStrategies: {
    isTypeOf: false
    resolveType: true
    __typename: false
  }
}

export interface NexusGenTypes {
  context: Context;
  inputTypes: NexusGenInputs;
  rootTypes: NexusGenRootTypes;
  inputTypeShapes: NexusGenInputs & NexusGenEnums & NexusGenScalars;
  argTypes: NexusGenArgTypes;
  fieldTypes: NexusGenFieldTypes;
  fieldTypeNames: NexusGenFieldTypeNames;
  allTypes: NexusGenAllTypes;
  typeInterfaces: NexusGenTypeInterfaces;
  objectNames: NexusGenObjectNames;
  inputNames: NexusGenInputNames;
  enumNames: NexusGenEnumNames;
  interfaceNames: NexusGenInterfaceNames;
  scalarNames: NexusGenScalarNames;
  unionNames: NexusGenUnionNames;
  allInputTypes: NexusGenTypes['inputNames'] | NexusGenTypes['enumNames'] | NexusGenTypes['scalarNames'];
  allOutputTypes: NexusGenTypes['objectNames'] | NexusGenTypes['enumNames'] | NexusGenTypes['unionNames'] | NexusGenTypes['interfaceNames'] | NexusGenTypes['scalarNames'];
  allNamedTypes: NexusGenTypes['allInputTypes'] | NexusGenTypes['allOutputTypes']
  abstractTypes: NexusGenTypes['interfaceNames'] | NexusGenTypes['unionNames'];
  abstractTypeMembers: NexusGenAbstractTypeMembers;
  objectsUsingAbstractStrategyIsTypeOf: NexusGenObjectsUsingAbstractStrategyIsTypeOf;
  abstractsUsingStrategyResolveType: NexusGenAbstractsUsingStrategyResolveType;
  features: NexusGenFeaturesConfig;
}


declare global {
  interface NexusGenPluginTypeConfig<TypeName extends string> {
  }
  interface NexusGenPluginInputTypeConfig<TypeName extends string> {
  }
  interface NexusGenPluginFieldConfig<TypeName extends string, FieldName extends string> {
  }
  interface NexusGenPluginInputFieldConfig<TypeName extends string, FieldName extends string> {
  }
  interface NexusGenPluginSchemaConfig {
  }
  interface NexusGenPluginArgConfig {
  }
}