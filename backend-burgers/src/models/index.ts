import Afdeling from "./Afdeling";
import Afspraak from "./Afspraak";
import Banktransactie, {PagedBanktransactie} from "./Banktransactie";
import Betaalinstructie from "./Betaalinstructie";
import Burger from "./Burger";
import Journaalpost from "./Journaalpost";
import Organisatie from "./Organisatie";
import PageInfo from "./PageInfo";
import Rekening from "./Rekening";

export {
	Afdeling,
	Afspraak,
	Banktransactie,
	PagedBanktransactie,
	Betaalinstructie,
	Burger,
	Journaalpost,
	Organisatie,
	Rekening,
	PageInfo,
};
